//
//  FavoritesViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 11..
//
//

#import "FavoritesViewController.h"
#import "ContactHeaderView.h"
#import "ContactSectionView.h"
#import "ContactScreenshotsView.h"
#import "ContactDestriptionView.h"
#import "ContactsService.h"
#import "UIColor+Hex.h"

typedef enum {
    ContactContentsTypeDetails,
    ContactContentsTypeReviews,
    ContactContentsTypeMapped
} ContactContentsType;

@interface FavoritesViewController ()<ContactsServiceDelegate> {
    IBOutlet UITableView* contactTableView;
    ContactHeaderView* headerView;
    ContactSectionView* sectionView;
    ContactScreenshotsView* screenshotsView;
    ContactDestriptionView* destriptionView;
    ContactContentsType contactContentsType;
    ContactsService* service;
    NSString* contactDescription;
    CGFloat contactDescriptionHeight;
}

@end

@implementation FavoritesViewController

- (id)init {
    self = [super init];
    if (self) {
        @autoreleasepool {
            UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:0];
            [self setTabBarItem:tabBarItem];
            
            headerView = [ContactHeaderView new];
            sectionView = [ContactSectionView new];
            screenshotsView = [[ContactScreenshotsView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 400)];
            destriptionView = [ContactDestriptionView new];
            
            contactContentsType = ContactContentsTypeDetails;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [contactTableView setTableHeaderView:headerView];
    
    service = [ContactsService new];
    [service setDelegate:self];
    [service loadContact:3];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (contactContentsType) {
        case ContactContentsTypeDetails:
            if (indexPath.row == 0) {
                return CGRectGetHeight(screenshotsView.frame);
            } else if (indexPath.row == 1) {
                return CGRectGetHeight(destriptionView.frame);
            }
            
        default:
            return 50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return sectionView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    @autoreleasepool {
        if (sectionView.frame.origin.y == 150 && sectionView.backgroundColor != [UIColor clearColor]) {
            [sectionView setBackgroundColor:[UIColor clearColor]];
        } else if (sectionView.frame.origin.y > 150 && sectionView.backgroundColor == [UIColor clearColor]) {
            [sectionView setBackgroundColor:[UIColor colorWithHex:@"#f7f7f7"]];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (contactContentsType) {
        case ContactContentsTypeDetails:
            return 2;
            
        default:
            return 50;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"%ld", (long)section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
//    [cell.textLabel setText:[NSString stringWithFormat:@"review %ld", (long)indexPath.row]];
    [self contactTableViewCell:cell cellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)contactTableViewCell:(UITableViewCell *)cell cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    for (UIView* subView in cell.subviews) {
        if ([subView isKindOfClass:[ContactScreenshotsView class]] || [subView isKindOfClass:[ContactDestriptionView class]]) {
            [subView removeFromSuperview];
        }
        NSLog(@"%@", subView.class);
    }
    switch (contactContentsType) {
        case ContactContentsTypeDetails:
            if (indexPath.row == 0) {
                [cell addSubview:screenshotsView];
            } else if (indexPath.row == 1) {
                [cell addSubview:destriptionView];
            }
            break;
            
        default:
            break;
    }
}

- (void)service:(ContactsService *)service didLoadContact:(Contact *)contact {
//    CGSize size = [contact.desc sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(CGRectGetWidth(self.view.frame), CGFLOAT_MAX)];
//    NSLog(@"%@, %f", contact.desc, size.height);
//    [destriptionView setFrame:CGRectMake(0, 0, CGRectGetWidth(destriptionView.frame), size.height)];
    [destriptionView setDesc:contact.desc];
    [contactTableView reloadData];
}

@end
