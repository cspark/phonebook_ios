//
//  AppDelegate.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 11..
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
