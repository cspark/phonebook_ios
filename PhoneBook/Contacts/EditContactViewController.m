//
//  EditContactViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 21..
//
//

#import "EditContactViewController.h"

@interface EditContactViewController ()<UIActionSheetDelegate> {
    IBOutlet UITextField* nameTextField;
    IBOutlet UITextField* phoneTextField;
}

@end

@implementation EditContactViewController

- (id)initWithDelegate:(id<EditContactViewControllerDelegate>) delegate Contact:(Contact *)contact {
    self = [super init];
    if (self) {
        @autoreleasepool {
            [self setDelegate:delegate];
            [self setContact:contact];
            
            UIBarButtonItem* cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didClickCancelBarButtonItem:)];
            [self.navigationItem setLeftBarButtonItem:cancelBarButtonItem];
            UIBarButtonItem* doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didClickDoneBarButtonItem:)];
            [self.navigationItem setRightBarButtonItem:doneBarButtonItem];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [nameTextField setText:_contact.name];
}

#pragma mark - Button Event

- (void)didClickCancelBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void)didClickDoneBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [_contact setName:nameTextField.text];
    [self performSelectorInBackground:@selector(requestEditContact) withObject:nil];
}

- (IBAction)didClickDeleteButton:(UIButton *)button {
    UIActionSheet* sheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"취소" destructiveButtonTitle:@"연락처 삭제" otherButtonTitles:nil, nil];
    [sheet showInView:self.view];
}

- (void)requestEditContact {
    @autoreleasepool {
        NSString *bodyString = [NSString stringWithFormat:@"no=%ld&name=%@", (unsigned long)_contact.no, _contact.name];
        NSMutableURLRequest* request = [NSMutableURLRequest new];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts?%@", URL, bodyString]]];
        [request setHTTPMethod:@"PUT"];
        
        NSURLResponse* response = nil;
        NSError* error = nil;
        NSData* responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error != nil) {
            NSLog(@"%@", error);
            return;
        }
        
        BOOL isSuccessResponse = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding].boolValue;
        if (isSuccessResponse) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:nil];
                if (_delegate != nil && [_delegate respondsToSelector:@selector(editContact:)]) {
                    [_delegate editContact:_contact];
                }
            });
        } else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"알림" message:@"수정에 실패하였습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

- (void)requestDeleteContact {
    @autoreleasepool {
        NSMutableURLRequest* request = [NSMutableURLRequest new];
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts?no=%ld", URL, (unsigned long)_contact.no]]];
        [request setHTTPMethod:@"DELETE"];
        
        NSURLResponse* response = nil;
        NSError* error = nil;
        NSData* responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error != nil) {
            NSLog(@"%@", error);
            return;
        }
        
        BOOL isSuccessResponse = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding].boolValue;
        if (isSuccessResponse) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissViewControllerAnimated:NO completion:nil];
                if (_delegate != nil && [_delegate respondsToSelector:@selector(deleteContact:)]) {
                    [_delegate deleteContact:_contact];
                }
            });
        } else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"알림" message:@"삭제에 실패하였습니다." delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelectorInBackground:@selector(requestDeleteContact) withObject:nil];
    }
}

@end
