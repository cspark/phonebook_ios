//
//  NewContactViewController.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 18..
//
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@protocol NewContactViewControllerDelegate;

@interface NewContactViewController : UIViewController

@property (nonatomic, assign) id<NewContactViewControllerDelegate> delegate;

- (id)initWithDelegate:(id<NewContactViewControllerDelegate>)delegate;

@end

@protocol NewContactViewControllerDelegate <NSObject>

- (void)newContact:(Contact *)contact;

@end
