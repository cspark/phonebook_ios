//
//  ScreenshotsViewController.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 29..
//
//

#import <UIKit/UIKit.h>

@interface ScreenshotsViewController : UIViewController

- (id)initWithPosition:(NSUInteger)position;

@end
