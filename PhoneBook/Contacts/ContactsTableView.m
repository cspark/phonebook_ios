//
//  ContactsTableView.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "ContactsTableView.h"

@interface ContactsTableView()<UITableViewDelegate, UITableViewDataSource> {
}
@end

@implementation ContactsTableView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDelegate:self];
        [self setDataSource:self];
    }
    return self;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_cellDelegate != nil && [_cellDelegate respondsToSelector:@selector(contactsTableView:didSelectContact:)]) {
        [_cellDelegate contactsTableView:(ContactsTableView *)tableView didSelectContact:[_contacts objectAtIndex:indexPath.row]];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* CellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    @autoreleasepool {
        Contact* contact = [_contacts objectAtIndex:indexPath.row];
        [cell.textLabel setText:contact.name];
    }
    return cell;
}

@end
