//
//  ScreenshotsViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 29..
//
//

#import "ScreenshotsViewController.h"

@interface ScreenshotsViewController ()

@end

@implementation ScreenshotsViewController

- (id)initWithPosition:(NSUInteger)position {
    self = [super init];
    if (self) {
        @autoreleasepool {
            NSLog(@"position : %lu", (unsigned long)position);
            UIBarButtonItem* doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didClickDoneBarButtonItem:)];
            [self.navigationItem setRightBarButtonItem:doneBarButtonItem];
        }
    }
    return self;
}

#pragma mark - Button Event

- (void)didClickDoneBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
