//
//  ContactsViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 11..
//
//

#import "ContactsViewController.h"
#import "ContactViewController.h"
#import "NewContactViewController.h"
#import "AreasViewController.h"
#import "ContactsTableView.h"
#import "ContactsService.h"
#import "Contacts.h"

@interface ContactsViewController ()<NewContactViewControllerDelegate, ContactViewControllerDelegate, ContactsTableViewCellDelegate, ContactsServiceDelegate> {
    IBOutlet ContactsTableView* contactsTableView;
    ContactsService* service;
}

@end

@implementation ContactsViewController

- (id)init {
    self = [super init];
    if (self) {
        [self initView];
        service = [ContactsService new];
        [service setDelegate:self];
    }
    return self;
}

- (void)initView {
    @autoreleasepool {
        [self setTitle:@"All Contacts"];
        
        UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemContacts tag:2];
        [self setTabBarItem:tabBarItem];
        
        UIBarButtonItem* areaBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Areas" style:UIBarButtonItemStylePlain target:self action:@selector(didClickAreaBarButtonItem:)];
        [self.navigationItem setLeftBarButtonItem:areaBarButtonItem];
        
        UIBarButtonItem* addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(didClickAddBarButtonItem:)];
        [self.navigationItem setRightBarButtonItem:addBarButtonItem];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [contactsTableView setCellDelegate:self];
    [service loadContacts];
}

#pragma mark - Button Event

- (void)didClickAreaBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [self modalAreasViewController];
}

- (void)didClickAddBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [self modalNewContactViewController];
}

#pragma mark - NewContactViewControllerDelegate

- (void)newContact:(Contact *)contact {
    [service newContact:contact];
    [self pushContactViewController:contact animated:NO];
}

#pragma mark - ContactViewControllerDelegate

- (void)reloadContacts {
    [service loadContacts];
}

#pragma mark - ContactsTableViewCellDelegate

- (void)contactsTableView:(ContactsTableView *)tableView didSelectContact:(Contact *)contact {
    [self pushContactViewController:contact animated:YES];
}

#pragma mark - ContactsServiceDelegate

- (void)service:(ContactsService *)contactsService didLoadContacts:(Contacts *)contacts {
    [contactsTableView setContacts:contacts];
    [contactsTableView reloadData];
}

- (void)service:(ContactsService *)contactsService didNewContact:(BOOL)isSuccess {
    if (isSuccess) {
        [service loadContacts];
    }
}

- (void)service:(ContactsService *)service didError:(NSError *)error {
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"알림" message:error.domain delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark - Private Methods

- (void)pushContactViewController:(Contact *)contact animated:(BOOL)animated {
    ContactViewController* contactViewController = [[ContactViewController alloc] initWithDelegate:self Contact:contact];
    [self.navigationController pushViewController:contactViewController animated:animated];
}

- (void)modalNewContactViewController {
    NewContactViewController* newContactViewController = [[NewContactViewController alloc] initWithDelegate:self];
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:newContactViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)modalAreasViewController {
    AreasViewController* areasViewController = [AreasViewController new];
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:areasViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

@end
