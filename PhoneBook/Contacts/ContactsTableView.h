//
//  ContactsTableView.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import <UIKit/UIKit.h>
#import "Contacts.h"

@protocol ContactsTableViewCellDelegate;

@interface ContactsTableView : UITableView

@property(nonatomic, assign) id<ContactsTableViewCellDelegate> cellDelegate;
@property(nonatomic, retain) Contacts* contacts;

@end

@protocol ContactsTableViewCellDelegate <NSObject>

- (void)contactsTableView:(ContactsTableView *)tableView didSelectContact:(Contact *)contact;

@end