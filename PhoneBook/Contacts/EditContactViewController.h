//
//  EditContactViewController.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 21..
//
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@protocol EditContactViewControllerDelegate;

@interface EditContactViewController : UIViewController

@property (nonatomic, assign) id<EditContactViewControllerDelegate> delegate;
@property (nonatomic, retain) Contact* contact;

- (id)initWithDelegate:(id<EditContactViewControllerDelegate>) delegate Contact:(Contact *)contact;

@end

@protocol EditContactViewControllerDelegate <NSObject>

- (void)editContact:(Contact *)contact;
- (void)deleteContact:(Contact *)contact;

@end
