//
//  NewContactViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 18..
//
//

#import "NewContactViewController.h"
#import "ContactViewController.h"

@interface NewContactViewController () {
    IBOutlet UITextField* nameTextField;
    IBOutlet UITextField* phoneTextField;
}

@end

@implementation NewContactViewController

- (id)initWithDelegate:(id<NewContactViewControllerDelegate>)delegate {
    self = [super init];
    if (self) {
        @autoreleasepool {
            [self setDelegate:delegate];
            [self setTitle:@"New Contact"];
            UIBarButtonItem* cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didClickCancelBarButtonItem:)];
            [self.navigationItem setLeftBarButtonItem:cancelBarButtonItem];
            UIBarButtonItem* doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didClickDoneBarButtonItem:)];
            [self.navigationItem setRightBarButtonItem:doneBarButtonItem];
        }
        
    }
    return self;
}

#pragma mark - Button Event

- (void)didClickCancelBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)didClickDoneBarButtonItem:(UIBarButtonItem *)barButtonItem {
    @autoreleasepool {
        [self didClickCancelBarButtonItem:nil];
        
        if (_delegate != nil && [_delegate respondsToSelector:@selector(newContact:)]) {
            Contact* contact = [Contact new];
            [contact setName:nameTextField.text];
            [_delegate newContact:contact];
        }
    }
}


@end
