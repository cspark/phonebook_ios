//
//  ContactsService.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import <Foundation/Foundation.h>
#import "Contacts.h"

@protocol ContactsServiceDelegate;

@interface ContactsService : NSObject

@property(nonatomic, assign) id<ContactsServiceDelegate> delegate;

- (void)loadContacts;
- (void)loadContact:(NSUInteger)no;
- (void)newContact:(Contact *)contact;

@end

@protocol ContactsServiceDelegate <NSObject>

@optional
- (void)service:(ContactsService *)service didLoadContacts:(Contacts *)contacts;
- (void)service:(ContactsService *)service didLoadContact:(Contact *)contact;
- (void)service:(ContactsService *)service didNewContact:(BOOL)isSuccess;
- (void)service:(ContactsService *)service didError:(NSError *)error;

@end