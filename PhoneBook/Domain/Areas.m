//
//  Areas.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 26..
//
//

#import "Areas.h"

@interface Areas() {
    NSMutableArray* areas;
}
@end

@implementation Areas

- (id)init {
    self = [super init];
    if (self) {
        areas = [NSMutableArray new];
    }
    return self;
}

- (Area *)objectAtIndex:(NSUInteger)index {
    return [areas objectAtIndex:index];
}

- (void)addObject:(Area *)area {
    [areas addObject:area];
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    [areas removeObjectAtIndex:index];
}

- (void)removeAllObjects {
    [areas removeAllObjects];
}

- (NSUInteger)count {
    return areas.count;
}

@end
