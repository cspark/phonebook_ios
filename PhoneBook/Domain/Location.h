//
//  Location.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 29..
//
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

@property(nonatomic, retain) NSString* address;
@property(nonatomic, assign) double latitude;
@property(nonatomic, assign) double longitude;

@end
