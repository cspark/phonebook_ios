//
//  ContactsService.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "ContactsService.h"
#import "LoadContactsBackgroundWork.h"
#import "LoadContactBackgroundWork.h"
#import "NewContactBackgroundWork.h"

@interface ContactsService() <BackgroundWorkDelegate>

@end

@implementation ContactsService

#pragma mark - BackgroundWork Request

- (void)loadContacts {
    LoadContactsBackgroundWork* getContactsBackgroundWork = [LoadContactsBackgroundWork new];
    [getContactsBackgroundWork setDelegate:self];
    [getContactsBackgroundWork request];
}

- (void)loadContact:(NSUInteger)no {
    LoadContactBackgroundWork* getContactsBackgroundWork = [[LoadContactBackgroundWork alloc] initWithContactNo:no];
    [getContactsBackgroundWork setDelegate:self];
    [getContactsBackgroundWork request];
}

- (void)newContact:(Contact *)contact {
    NewContactBackgroundWork* newContactBackgroundWork = [NewContactBackgroundWork new];
    [newContactBackgroundWork setDelegate:self];
    [newContactBackgroundWork requestWithName:contact.name phone:@""];
}

#pragma mark - BackgroundWorkDelegate

- (void)backgroundWork:(BackgroundWork *)backgroundWork result:(id)result error:(NSError *)error {
    if (error == nil) {
        if ([backgroundWork isKindOfClass:[LoadContactsBackgroundWork class]]) {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(service:didLoadContacts:)]) {
                [_delegate service:self didLoadContacts:result];
            }
        }
        
        if ([backgroundWork isKindOfClass:[LoadContactBackgroundWork class]]) {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(service:didLoadContact:)]) {
                [_delegate service:self didLoadContact:result];
            }
        }
        
        if ([backgroundWork isKindOfClass:[NewContactBackgroundWork class]]) {
            if (_delegate != nil && [_delegate respondsToSelector:@selector(service:didNewContact:)]) {
                [_delegate service:self didNewContact:(BOOL)[result objectForKey:@"result"]];
            }
        }
    } else {
        if (_delegate != nil && [_delegate respondsToSelector:@selector(service:didError:)]) {
            [_delegate service:self didError:error];
        }
    }
}

@end
