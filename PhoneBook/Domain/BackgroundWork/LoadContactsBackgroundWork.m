//
//  GetContactsBackgroundWork.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "LoadContactsBackgroundWork.h"
#import "Contacts.h"

@interface LoadContactsBackgroundWork() {
    Contacts* contacts;
}
@end

@implementation LoadContactsBackgroundWork

- (id)init {
    self = [super init];
    if (self) {
        contacts = [Contacts new];
    }
    return self;
}

- (void)request {
    @autoreleasepool {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts", URL]];
            [network requestURL:url paramters:nil httpMethodType:GET];
        });
    }
}

#pragma mark - NetworkDelegate

- (void)network:(Network *)network result:(id)result error:(NSError *)error {
    @autoreleasepool {
        if (error == nil) {
            [contacts removeAllObjects];
            Contact* contact = nil;
            for (NSDictionary* dic in (NSArray *)result) {
                contact = [Contact new];
                [contact setNo:[[dic objectForKey:@"no"] integerValue]];
                [contact setName:[dic objectForKey:@"name"]];
                [contacts addObject:contact];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self backgroundWork:self result:contacts error:error];
        });
    }
}

@end
