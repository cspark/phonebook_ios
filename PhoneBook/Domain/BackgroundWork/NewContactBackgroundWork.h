//
//  NewContactBackgroundWork.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "BackgroundWork.h"

@interface NewContactBackgroundWork : BackgroundWork

- (void)requestWithName:(NSString *)name phone:(NSString *)phone;

@end
