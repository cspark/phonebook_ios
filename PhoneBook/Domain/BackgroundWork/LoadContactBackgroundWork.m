//
//  LoadContactBackgroundWork.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 29..
//
//

#import "LoadContactBackgroundWork.h"
#import "Contact.h"

@interface LoadContactBackgroundWork() {
    NSUInteger contactNo;
}

@end

@implementation LoadContactBackgroundWork

- (id)initWithContactNo:(NSUInteger)no {
    self = [super init];
    if (self) {
        contactNo = no;
    }
    return self;
}

- (void)request {
    @autoreleasepool {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/%ld", URL, contactNo]];
            [network requestURL:url paramters:nil httpMethodType:GET];
        });
    }
}

#pragma mark - NetworkDelegate

- (void)network:(Network *)network result:(id)result error:(NSError *)error {
    @autoreleasepool {
        Location* location = nil;
        Contact* contact = nil;
        if (error == nil) {
            NSDictionary* locationDic = [result objectForKey:@"location"];
            location = [Location new];
            [location setAddress:[locationDic objectForKey:@"address"]];
            [location setLatitude:[[locationDic objectForKey:@"latitude"] doubleValue]];
            [location setLongitude:[[locationDic objectForKey:@"longitude"] doubleValue]];
            
            contact = [Contact new];
            [contact setNo:[[result objectForKey:@"no"] integerValue]];
            [contact setName:[result objectForKey:@"name"]];
            [contact setDesc:[result objectForKey:@"description"]];
            [contact setPhones:[result objectForKey:@"phones"]];
            [contact setScreenshots:[result objectForKey:@"screenshots"]];
            [contact setLocation:location];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self backgroundWork:self result:contact error:error];
        });
    }
}

@end
