//
//  LoadContactBackgroundWork.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 29..
//
//

#import "BackgroundWork.h"

@interface LoadContactBackgroundWork : BackgroundWork

- (id)initWithContactNo:(NSUInteger)no;

@end
