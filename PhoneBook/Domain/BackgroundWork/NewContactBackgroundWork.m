//
//  NewContactBackgroundWork.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "NewContactBackgroundWork.h"

@implementation NewContactBackgroundWork

- (void)requestWithName:(NSString *)name phone:(NSString *)phone {
    @autoreleasepool {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/contacts/", URL]];
            NSString *paramters = [NSString stringWithFormat:@"name=%@&phone=%@", name, phone];
            [network requestURL:url paramters:paramters httpMethodType:POST];
        });
    }
}

#pragma mark - NetworkDelegate

- (void)network:(Network *)network result:(id)result error:(NSError *)error {
    @autoreleasepool {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self backgroundWork:self result:result error:error];
        });
    }
}

@end
