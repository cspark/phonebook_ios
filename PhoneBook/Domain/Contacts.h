//
//  Contacts.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 18..
//
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@interface Contacts : NSObject

- (Contact *)objectAtIndex:(NSUInteger)index;
- (void)addObject:(Contact *)contact;
- (void)removeObjectAtIndex:(NSUInteger)index;
- (void)removeAllObjects;
- (NSUInteger)count;

@end
