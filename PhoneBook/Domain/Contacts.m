//
//  Contacts.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 18..
//
//

#import "Contacts.h"

@interface Contacts() {
    NSMutableArray* contacts;
}

@end

@implementation Contacts

- (id)init
{
    self = [super init];
    if (self) {
        contacts = [NSMutableArray new];
    }
    return self;
}

- (Contact *)objectAtIndex:(NSUInteger)index {
    return [contacts objectAtIndex:index];
}

- (void)addObject:(Contact *)contact {
    [contacts addObject:contact];
}

- (void)removeObjectAtIndex:(NSUInteger)index {
    [contacts removeObjectAtIndex:index];
}

- (void)removeAllObjects {
    [contacts removeAllObjects];
}

- (NSUInteger)count {
    return contacts.count;
}

@end
