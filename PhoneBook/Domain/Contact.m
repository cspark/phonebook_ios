//
//  Contact.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 12..
//
//

#import "Contact.h"

@implementation Contact

- (NSString *)description {
    return [NSString stringWithFormat:@"Contact [no=%ld, name=%@, location=%@, description=%@, phones=%@, screenshots=%@]", _no, _name, _location, _desc, _phones, _screenshots];
}

@end
