//
//  Areas.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 26..
//
//

#import <Foundation/Foundation.h>
#import "Area.h"

@interface Areas : NSObject

- (Area *)objectAtIndex:(NSUInteger)index;
- (void)addObject:(Area *)area;
- (void)removeObjectAtIndex:(NSUInteger)index;
- (void)removeAllObjects;
- (NSUInteger)count;

@end
