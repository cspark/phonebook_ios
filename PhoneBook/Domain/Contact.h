//
//  Contact.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 12..
//
//

#import <Foundation/Foundation.h>
#import "Location.h"

@interface Contact : NSObject

@property (nonatomic, assign) NSUInteger no;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) Location* location;
@property (nonatomic, retain) NSArray* phones;
@property (nonatomic, retain) NSArray* screenshots;

@end
