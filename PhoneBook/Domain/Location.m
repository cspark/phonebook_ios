//
//  Location.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 29..
//
//

#import "Location.h"

@implementation Location

- (NSString *)description {
    return [NSString stringWithFormat:@"Location [address=%@, latitude=%f, longitude=%f]", _address, _latitude, _longitude];
}

@end
