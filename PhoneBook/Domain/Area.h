//
//  Area.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 26..
//
//

#import <Foundation/Foundation.h>

@interface Area : NSObject

@property (nonatomic, retain) NSString* code;
@property (nonatomic, retain) NSString* name;

- (Area *)areaAtIndex:(NSUInteger)index;
- (NSUInteger)childCount;

@end
