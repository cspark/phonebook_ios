//
//  AreasViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 26..
//
//

#import "AreasViewController.h"

@interface AreasViewController () {
    UIBarButtonItem* cancelBarButtonItem;
    UIBarButtonItem* addBarButtonItem;
    IBOutlet UITableView* areasTableView;
}

@end

@implementation AreasViewController

- (id)init {
    self = [super init];
    if (self) {
        @autoreleasepool {
            UIBarButtonItem* editBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(didClickEditBarButtonItem:)];
            [self.navigationItem setLeftBarButtonItem:editBarButtonItem];
            
            cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didClickCancelBarButtonItem:)];
            [self.navigationItem setRightBarButtonItem:cancelBarButtonItem];
            
            addBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(didClickAddBarButtonItem:)];
        }
    }
    return self;
}

#pragma mark - Button Event

- (void)didClickEditBarButtonItem:(UIBarButtonItem *)barButtonItem {
    @autoreleasepool {
        [areasTableView setEditing:!areasTableView.editing];
        if (areasTableView.editing) {
            [self.navigationItem setRightBarButtonItem:addBarButtonItem];
        } else {
            [self.navigationItem setRightBarButtonItem:cancelBarButtonItem];
        }
    }
}

- (void)didClickCancelBarButtonItem:(UIBarButtonItem *)barButtonItem {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didClickAddBarButtonItem:(UIBarButtonItem *)barButtonItem {
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"지역 추가" message:@"지역명을 입력해 주세요." delegate:nil cancelButtonTitle:@"취소" otherButtonTitles:@"확인", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [alertView show];
}

@end
