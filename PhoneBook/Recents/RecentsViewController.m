//
//  RecentsViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 11..
//
//

#import "RecentsViewController.h"

@interface RecentsViewController ()

@end

@implementation RecentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        @autoreleasepool {
            UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemRecents tag:1];
            [self setTabBarItem:tabBarItem];
        }
    }
    return self;
}

@end
