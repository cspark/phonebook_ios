//
//  SettingsViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 11..
//
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        @autoreleasepool {
            UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:4];
            [self setTabBarItem:tabBarItem];
        }
    }
    return self;
}

@end
