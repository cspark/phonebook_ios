//
//  BackgroundWork.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "BackgroundWork.h"

@implementation BackgroundWork

- (id)init {
    self = [super init];
    if (self) {
        network = [[Network alloc] initWithDelegate:self];
    }
    return self;
}

#pragma mark - NetworkDelegate

- (void)network:(Network *)network result:(id)result error:(NSError *)error {
}

- (void)request {
}

- (void)backgroundWork:(BackgroundWork *)backgroundWork result:(id)result error:(NSError *)error {
    if (_delegate != nil && [_delegate respondsToSelector:@selector(backgroundWork:result:error:)]) {
        [_delegate backgroundWork:backgroundWork result:result error:error];
    }
}

@end
