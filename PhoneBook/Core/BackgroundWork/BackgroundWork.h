//
//  BackgroundWork.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import <Foundation/Foundation.h>
#import "Network.h"

@protocol BackgroundWorkDelegate;

@interface BackgroundWork : NSObject <NetworkDelegate> {
    @protected
    Network* network;
}

@property(nonatomic, assign) id<BackgroundWorkDelegate> delegate;

- (void)request;
- (void)backgroundWork:(BackgroundWork *)backgroundWork result:(id)result error:(NSError *)error;

@end

@protocol BackgroundWorkDelegate <NSObject>

- (void)backgroundWork:(BackgroundWork *)backgroundWork result:(id)result error:(NSError *)error;

@end