//
//  Network.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import <Foundation/Foundation.h>

typedef enum {
    GET, POST, PUT, DELETE
} HttpMethodType;

@protocol NetworkDelegate;

@interface Network : NSObject

@property (nonatomic, assign) id<NetworkDelegate> delegate;
@property (nonatomic, retain) NSMutableURLRequest* request;
@property (nonatomic, assign) HttpMethodType httpMethodType;

- (id)initWithDelegate:(id<NetworkDelegate>)delegate;
- (void)requestURL:(NSURL *)url paramters:(NSString *)paramters httpMethodType:(HttpMethodType)httpMethodType;

@end

@protocol NetworkDelegate <NSObject>

- (void)network:(Network *)network result:(id)result error:(NSError *)error;

@end
