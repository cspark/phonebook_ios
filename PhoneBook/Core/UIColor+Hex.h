//
//  UIColor+UIColor_Hex.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 24..
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithHex:(NSString *)hex;

@end
