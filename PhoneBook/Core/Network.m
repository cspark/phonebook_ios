//
//  Network.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 22..
//
//

#import "Network.h"

@implementation Network

- (id)initWithDelegate:(id<NetworkDelegate>)delegate {
    self = [super init];
    if (self) {
        @autoreleasepool {
            [self setDelegate:delegate];
            _request = [NSMutableURLRequest new];
        }
    }
    return self;
}

- (void)requestURL:(NSURL *)url paramters:(NSString *)paramters httpMethodType:(HttpMethodType)httpMethodType {
    @autoreleasepool {
        NSString* method = [self httpMethod:httpMethodType];
        [_request setURL:url];
        [_request setHTTPMethod:method];
        if (httpMethodType == POST) {
            [_request setHTTPBody:[paramters dataUsingEncoding:NSUTF8StringEncoding]];
        } else if (httpMethodType != POST && paramters != nil) {
            [_request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?%@", url, paramters]]];
        }
        
        NSURLResponse* response = nil;
        NSError* error = nil;
        id result = nil;
        
        NSData* responseData = [NSURLConnection sendSynchronousRequest:_request returningResponse:&response error:&error];
        if (error != nil) {
            [self result:result error:error];
            return;
        }
        
        result = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        if (error != nil) {
            [self result:result error:error];
            return;
        }
        
        [self result:result error:error];
    }
}

- (void)result:(id)result error:(NSError *)error {
    if (_delegate != nil && [_delegate respondsToSelector:@selector(network:result:error:)]) {
        [_delegate network:self result:result error:error];
    }
}

- (NSString *)httpMethod:(HttpMethodType)httpMethodType {
    NSString* httpMethod = nil;
    switch (httpMethodType) {
        case GET:
            httpMethod = @"GET";
            break;
            
        case POST:
            httpMethod = @"POST";
            break;
            
        case PUT:
            httpMethod = @"PUT";
            break;
            
        case DELETE:
            httpMethod = @"DELETE";
            break;
            
        default:
            break;
    }
    return httpMethod;
}

@end
