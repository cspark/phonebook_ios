//
//  ContactViewController.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 19..
//
//

#import <UIKit/UIKit.h>
#import "ContactsService.h"

@protocol ContactViewControllerDelegate;

@interface ContactViewController : UIViewController

@property (nonatomic, assign) id<ContactViewControllerDelegate> delegate;
@property (nonatomic, retain) Contact* contact;

- (id)initWithDelegate:(id<ContactViewControllerDelegate>)delegate Contact:(Contact *)contact;

@end

@protocol ContactViewControllerDelegate <NSObject>

- (void)reloadContacts;

@end
