//
//  ContactTableViewFactory.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 23..
//
//

#import <Foundation/Foundation.h>
#import "ContactTableViewProtocol.h"
#import "Contact.h"

typedef enum {
    ContactTypeDetails,
    ContactTypeReviews,
    ContactTypeMapped
} ContactType;

@interface ContactTableViewFactory : NSObject

- (id<ContactTableViewProtocol>)create:(ContactType)type;

@end
