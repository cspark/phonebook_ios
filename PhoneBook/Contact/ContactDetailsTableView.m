//
//  ContactDetailsTableViewCell.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 23..
//
//

#import "ContactDetailsTableView.h"
#import "ContactScreenshotTableViewCell.h"
#import "ContactDescriptionTableViewCell.h"

@implementation ContactDetailsTableView

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 400;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell;
    if (indexPath.row == 0) {
        cell = [ContactScreenshotTableViewCell new];
    } else if (indexPath.row == 1) {
        cell = [ContactDescriptionTableViewCell new];
    } else {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    }
    return cell;
}

@end
