//
//  ContactViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 19..
//
//

#import "ContactViewController.h"
#import "EditContactViewController.h"
#import "ScreenshotsViewController.h"
#import "ContactHeaderView.h"
#import "ContactTableView.h"
#import "ContactsService.h"

@interface ContactViewController ()<ContactTableViewDelegate, EditContactViewControllerDelegate, ContactsServiceDelegate> {
    IBOutlet ContactTableView* contactTableView;
    ContactHeaderView* headerView;
    ContactsService* service;
}
@end

@implementation ContactViewController

- (id)initWithDelegate:(id<ContactViewControllerDelegate>)delegate Contact:(Contact *)contact {
    self = [super init];
    if (self) {
        [self setDelegate:delegate];
        [self setContact:contact];
        
        service = [ContactsService new];
        [service setDelegate:self];
        
        UIBarButtonItem* doneBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(didClickEditBarButtonItem:)];
        [self.navigationItem setRightBarButtonItem:doneBarButtonItem];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    headerView = [ContactHeaderView new];
    [contactTableView setTableHeaderView:headerView];
    [contactTableView setContactTableViewDelegate:self];
    
    [service loadContact:3];
}

- (void)bind {
    [headerView setName:_contact.name];
    [headerView setAddress:_contact.location.address];
    [headerView setPhones:_contact.phones];
    
    [contactTableView setContact:_contact];
    [contactTableView reloadData];
}

#pragma mark - Button Event

- (void)didClickEditBarButtonItem:(UIBarButtonItem *)barButtonItem {
    EditContactViewController* editContactViewController = [[EditContactViewController alloc] initWithDelegate:self Contact:_contact];
    UINavigationController* naviEditContactViewController = [[UINavigationController alloc] initWithRootViewController:editContactViewController];
    [self presentViewController:naviEditContactViewController animated:NO completion:nil];
}

#pragma mark - EditContactViewControllerDelegate

- (void)editContact:(Contact *)contact {
    _contact = contact;
    [self bind];
    if (_delegate != nil && [_delegate respondsToSelector:@selector(reloadContacts)]) {
        [_delegate reloadContacts];
    }
}

- (void)deleteContact:(Contact *)contact {
    [self.navigationController popViewControllerAnimated:YES];
    if (_delegate != nil && [_delegate respondsToSelector:@selector(reloadContacts)]) {
        [_delegate reloadContacts];
    }
}

#pragma mark - ContactTableViewDelegate

- (void)didTapDetailImageAtIndex:(NSUInteger)index {
    ScreenshotsViewController* screenshotsViewController = [[ScreenshotsViewController alloc] initWithPosition:index];
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:screenshotsViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark - ContactsServiceDelegate

- (void)service:(ContactsService *)service didLoadContact:(Contact *)contact {
    _contact = contact;
    [self bind];
}

- (void)service:(ContactsService *)service didError:(NSError *)error {
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"알림" message:error.domain delegate:nil cancelButtonTitle:@"확인" otherButtonTitles:nil, nil];
    [alertView show];
}

@end
