//
//  ContactDestriptionView.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 10. 2..
//
//

#import "ContactDestriptionView.h"

@interface ContactDestriptionView () {
    UITextView* destriptionTextView;
}

@end

@implementation ContactDestriptionView

- (id)init {
    self = [super init];
    if (self) {
        UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 17)];
        [label setText:@"Destription"];
        [self addSubview:label];
        
        destriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 320, 17)];
        [destriptionTextView setFont:[UIFont systemFontOfSize:14]];
        [destriptionTextView setBackgroundColor:[UIColor redColor]];
        [self addSubview:destriptionTextView];
    }
    return self;
}

- (void)setDesc:(NSString *)desc {
    CGSize size = [desc sizeWithFont:destriptionTextView.font constrainedToSize:CGSizeMake(320, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    NSLog(@"%@, %f", desc, size.height);
    [destriptionTextView setText:desc];
    NSLog(@"%f", destriptionTextView.contentSize.height);
    [destriptionTextView setFrame:CGRectMake(0, 20, 320, size.height)];
    [self setFrame:CGRectMake(0, 0, 320, size.height + 20)];
}

@end
