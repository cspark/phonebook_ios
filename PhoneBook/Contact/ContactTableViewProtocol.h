//
//  ContactTableViewCell.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 23..
//
//

#import <Foundation/Foundation.h>
#import "Contact.h"

@protocol ContactTableViewProtocol <NSObject>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end