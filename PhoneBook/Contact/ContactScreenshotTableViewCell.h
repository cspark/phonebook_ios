//
//  ContactDetailTableViewCell.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 28..
//
//

#import <UIKit/UIKit.h>

@protocol ContactScreenshotTableViewCellDelegate;

@interface ContactScreenshotTableViewCell : UITableViewCell

@property(nonatomic, assign) id<ContactScreenshotTableViewCellDelegate> delegate;

- (void)setScreenshots:(NSArray *)screenshots;

@end

@protocol ContactScreenshotTableViewCellDelegate <NSObject>

- (void)didTapDetailImageAtIndex:(NSUInteger)index;
- (void)contactDetailsTableViewWillBeginDragging:(UIScrollView *)scrollView;

@end
