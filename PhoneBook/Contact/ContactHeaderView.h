//
//  ContactHeaderView.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 27..
//
//

#import <UIKit/UIKit.h>

@interface ContactHeaderView : UIView

- (void)setName:(NSString *)name;
- (void)setPhones:(NSArray *)phones;
- (void)setAddress:(NSString *)address;

@end
