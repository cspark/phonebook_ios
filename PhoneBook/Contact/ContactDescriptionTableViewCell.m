//
//  ContactDescriptionTableViewCell.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 29..
//
//

#import "ContactDescriptionTableViewCell.h"

@interface ContactDescriptionTableViewCell() {
    IBOutlet UITextView* descriptionTextView;
}

@end

@implementation ContactDescriptionTableViewCell

- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ContactDescriptionTableViewCell" owner:self options:nil] objectAtIndex:0];
    if (self) {
    }
    return self;
}

- (void)setDesc:(NSString *)desc {
    [descriptionTextView setText:desc];
}

@end
