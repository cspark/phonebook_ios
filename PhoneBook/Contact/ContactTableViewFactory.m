//
//  ContactTableViewFactory.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 23..
//
//

#import "ContactTableViewFactory.h"
#import "ContactDetailsTableView.h"
#import "ContactReviewsTableView.h"
#import "ContactMappedTableView.h"

@implementation ContactTableViewFactory

- (id<ContactTableViewProtocol>)create:(ContactType)type {
    switch (type) {
        case ContactTypeDetails:
            return [ContactDetailsTableView new];
            
        case ContactTypeReviews:
            return [ContactReviewsTableView new];
            
        case ContactTypeMapped:
            return [ContactMappedTableView new];
            
        default:
            return nil;
    }
}

@end
