//
//  ContactDetailTableViewCell.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 28..
//
//

#import "ContactScreenshotTableViewCell.h"

@interface ContactScreenshotTableViewCell()<UIScrollViewDelegate> {
    IBOutlet UIScrollView* detailsScrollView;
}

@end

@implementation ContactScreenshotTableViewCell

- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ContactScreenshotTableViewCell" owner:self options:nil] objectAtIndex:0];
    if (self) {
    }
    return self;
}

- (void)setScreenshots:(NSArray *)screenshots {
    NSLog(@"setScreenshots : %ld, %@", detailsScrollView.subviews.count, screenshots);
    @autoreleasepool {
        for (UIView* subView in detailsScrollView.subviews) {
            [subView removeFromSuperview];
        }
        
        int width = CGRectGetWidth(self.frame);
        int height = CGRectGetHeight(self.frame);
        [detailsScrollView setDelegate:self];
        [detailsScrollView setContentSize:CGSizeMake(width * screenshots.count, height)];
        
        UIImageView* detailImageView;
        UITapGestureRecognizer* tapGestureRecognizer;
        for (int i = 0; i < screenshots.count; i++) {
            tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapDetailImageView:)];
            [tapGestureRecognizer setNumberOfTapsRequired:1];
            
            detailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * width, 0, width, height)];
            [detailImageView setContentMode:UIViewContentModeScaleAspectFit];
            [detailImageView setUserInteractionEnabled:YES];
            [detailImageView addGestureRecognizer:tapGestureRecognizer];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString* url = [screenshots objectAtIndex:i];
                NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                [detailImageView setImage:[UIImage imageWithData:data]];
            });
            
            [detailsScrollView addSubview:detailImageView];
        }
    }
}

- (void)didTapDetailImageView:(UITapGestureRecognizer *)tapGestureRecognizer {
    if (_delegate != nil && [_delegate respondsToSelector:@selector(didTapDetailImageAtIndex:)]) {
        [_delegate didTapDetailImageAtIndex:detailsScrollView.contentOffset.x / 320];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (_delegate != nil && [_delegate respondsToSelector:@selector(contactDetailsTableViewWillBeginDragging:)]) {
        [_delegate contactDetailsTableViewWillBeginDragging:scrollView];
    }
}

@end
