//
//  ContactDestriptionView.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 10. 2..
//
//

#import <UIKit/UIKit.h>

@interface ContactDestriptionView : UIView

- (void)setDesc:(NSString *)desc;

@end
