//
//  ContactTableView.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 22..
//
//

#import "ContactTableView.h"
#import "ContactSectionView.h"
#import "ContactTableViewFactory.h"
#import "ContactTableViewProtocol.h"
#import "ContactScreenshotTableViewCell.h"
#import "ContactDescriptionTableViewCell.h"
#import "UIColor+Hex.h"

@interface ContactTableView()<UITableViewDelegate, UITableViewDataSource, ContactSectionViewDelegate, ContactScreenshotTableViewCellDelegate> {
    ContactSectionView* sectionView;
    ContactTableViewFactory* factory;
    id<ContactTableViewProtocol> contactTableView;
}
@end

@implementation ContactTableView

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setDelegate:self];
        [self setDataSource:self];
        
        sectionView = [ContactSectionView new];
        [sectionView setDelegate:self];
        
        factory = [ContactTableViewFactory new];
        contactTableView = [factory create:ContactTypeDetails];
    }
    return self;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [contactTableView tableView:tableView heightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return sectionView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    @autoreleasepool {
        if (sectionView.frame.origin.y == 150 && sectionView.backgroundColor != [UIColor clearColor]) {
            [sectionView setBackgroundColor:[UIColor clearColor]];
        } else if (sectionView.frame.origin.y > 150 && sectionView.backgroundColor == [UIColor clearColor]) {
            [sectionView setBackgroundColor:[UIColor colorWithHex:@"#f7f7f7"]];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [contactTableView tableView:tableView numberOfRowsInSection:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"%ld", (long)section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [contactTableView tableView:tableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[ContactScreenshotTableViewCell class]]) {
        ContactScreenshotTableViewCell* screenshotsCell = (ContactScreenshotTableViewCell *) cell;
        [screenshotsCell setDelegate:self];
        [screenshotsCell setScreenshots:_contact.screenshots];
    } else if ([cell isKindOfClass:[ContactDescriptionTableViewCell class]]) {
        ContactDescriptionTableViewCell* descriptionCell = (ContactDescriptionTableViewCell *) cell;
        [descriptionCell setDesc:_contact.desc];
    }
    
    return cell;
}

#pragma mark - ContactSectionViewDelegate

- (void)didChangeSegmentedControlAtIndex:(NSUInteger)index {
    contactTableView = [factory create:(unsigned)index];
    [self reloadData];
}

#pragma mark - ContactScreenshotTableViewCellDelegate

- (void)didTapDetailImageAtIndex:(NSUInteger)index {
    if (_contactTableViewDelegate != nil && [_contactTableViewDelegate respondsToSelector:@selector(didTapDetailImageAtIndex:)]) {
        [_contactTableViewDelegate didTapDetailImageAtIndex:index];
    }
}

- (void)contactDetailsTableViewWillBeginDragging:(UIScrollView *)scrollView {
    if (self.contentOffset.y != 90) {
        [self setContentOffset:CGPointMake(self.contentOffset.x, 90) animated:YES];
    }
}

@end
