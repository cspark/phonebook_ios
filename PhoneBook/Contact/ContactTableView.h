//
//  ContactTableView.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 9. 22..
//
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@protocol ContactTableViewDelegate;

@interface ContactTableView : UITableView

@property (nonatomic, assign) id<ContactTableViewDelegate> contactTableViewDelegate;
@property (nonatomic, retain) Contact* contact;

@end

@protocol ContactTableViewDelegate <NSObject>

- (void)didTapDetailImageAtIndex:(NSUInteger)index;

@end