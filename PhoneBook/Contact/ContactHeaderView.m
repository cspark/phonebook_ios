//
//  ContactHeaderView.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 27..
//
//

#import "ContactHeaderView.h"

@interface ContactHeaderView() {
    IBOutlet UILabel* nameLabel;
    IBOutlet UILabel* phoneLabel;
    IBOutlet UILabel* addressLabel;
}

@end

@implementation ContactHeaderView

- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"ContactHeaderView" owner:self options:nil] objectAtIndex:0];
    if (self) {
    }
    return self;
}

- (void)setName:(NSString *)name {
    [nameLabel setText:name];
}

- (void)setPhones:(NSArray *)phones {
    [phoneLabel setText:[phones objectAtIndex:0]];
}

- (void)setAddress:(NSString *)address {
    [addressLabel setText:address];
}

@end
