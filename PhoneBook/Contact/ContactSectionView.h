//
//  ContactSectionView.h
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 27..
//
//

#import <UIKit/UIKit.h>

@protocol ContactSectionViewDelegate;

@interface ContactSectionView : UIView

@property(nonatomic, assign) id<ContactSectionViewDelegate> delegate;

@end

@protocol ContactSectionViewDelegate <NSObject>

- (void)didChangeSegmentedControlAtIndex:(NSUInteger)index;

@end
