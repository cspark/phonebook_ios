//
//  ContactScreenshotsView.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 10. 1..
//
//

#import "ContactScreenshotsView.h"

@interface ContactScreenshotsView () {
    UIScrollView* screenshotsScrollView;
}

@end

@implementation ContactScreenshotsView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        screenshotsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), CGRectGetHeight(frame))];
        [self addSubview:screenshotsScrollView];
    }
    return self;
}

@end
