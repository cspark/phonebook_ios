//
//  ContactSectionView.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 27..
//
//

#import "ContactSectionView.h"
#import "UIColor+Hex.h"

@interface ContactSectionView() {
    UISegmentedControl* segmentedControl;
    UIView* backgroundView;
}

@end

@implementation ContactSectionView

- (id)init {
    self = [super init];
    if (self) {
        @autoreleasepool {
            backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
            [backgroundView setBackgroundColor:[UIColor clearColor]];
            [self addSubview:backgroundView];
            
            segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Details", @"Reviews", @"Mapped"]];
            [segmentedControl setFrame:CGRectMake(10, 10, 300, 29)];
            [segmentedControl setSelectedSegmentIndex:0];
            [segmentedControl setTintColor:[UIColor lightGrayColor]];
            [segmentedControl addTarget:self action:@selector(didChangeSegmentedControl:) forControlEvents:UIControlEventValueChanged];
            [self addSubview:segmentedControl];
            
            UIView* lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 49.5, 320, 0.5)];
            [lineView setBackgroundColor:[UIColor colorWithHex:@"#b0b0b0"]];
            [self addSubview:lineView];
        }
    }
    return self;
}

- (void)didChangeSegmentedControl:(UISegmentedControl *)sender {
    if (_delegate != nil && [_delegate respondsToSelector:@selector(didChangeSegmentedControlAtIndex:)]) {
        [_delegate didChangeSegmentedControlAtIndex:sender.selectedSegmentIndex];
    }
}

- (UIColor *)backgroundColor {
    return backgroundView.backgroundColor;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor {
    [backgroundView setBackgroundColor:backgroundColor];
    [backgroundView setAlpha:0.9];
}

@end
