//
//  HomeViewController.m
//  PhoneBook
//
//  Created by 박찬순 on 2014. 8. 11..
//
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        @autoreleasepool {
            UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMostViewed tag:3];
            [self setTabBarItem:tabBarItem];
        }
    }
    return self;
}

@end
